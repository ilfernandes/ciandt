package br.com.treinamento.apimarvelclient;

import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.treinamento.config.AppConfig;
import br.com.treinamento.marvelclient.MarvelClient;
import br.com.treinamento.model.Characters;
import br.com.treinamento.model.Comics;
import br.com.treinamento.model.Result;
import br.com.treinamento.persistencia.Banco;

@RunWith(SpringJUnit4ClassRunner.class) // Indicates that the class should use
										// Spring's JUnit facilities
@ContextConfiguration(classes = AppConfig.class)
@WebAppConfiguration
public class TestApiMarvel {

	@Autowired
	private WebApplicationContext context;

	@Autowired
	private Banco db;

	@Autowired
	private MarvelClient client;

	private MockMvc mock;

	@Before
	public void setUp() {
		mock = MockMvcBuilders.webAppContextSetup(context).build();
	}

	@After
	public void after() {
		db.deleteAll();
	}

	@Test
	public void testarTypeResolver() throws Exception {
		ObjectMapper m = new ObjectMapper();
		String path = this.getClass().getResource("/").getPath() + "character.json";
		Characters readValue = (Characters) m.readValue(new File(path), Result.class);
		assertThat(readValue, Matchers.notNullValue());
		path = this.getClass().getResource("/").getPath() + "comics.json";
		Comics readValue2 = (Comics) m.readValue(new File(path), Result.class);
		assertThat(readValue2, Matchers.notNullValue());
	}

	@Test
	public void testaApiMarvelClient() throws Exception {
		db.adicionarTodos(client.result("characters"));
		List<Characters> personagens = db.obterTodosPersonagens();
		assertThat(personagens, Matchers.notNullValue());
		assertThat(personagens.size(), Matchers.equalTo(3));
	}

	@Test
	public void testaChamadaCallMarvel() throws Exception {
		assertThat(mock, Matchers.notNullValue());
		mock.perform(get("/callMarvel")).andExpect(status().isOk());
		List<Characters> personagens = db.obterTodosPersonagens();
		List<Comics> comics = db.obterTodosComics();
		assertThat(personagens.size(), Matchers.equalTo(3));
		assertThat(comics.size(), Matchers.equalTo(3));
	}

	@Test
	public void testaRestGet() throws Exception {
		Characters personagem = new Characters();
		personagem.setId(1);
		personagem.setName("Nome");
		db.adicionarPersonagem(personagem);
		MvcResult result = mock.perform(get("/personagens")).andExpect(status().isOk()).andReturn();
		assertThat(result.getResponse().getContentAsString(), Matchers.containsString("Nome"));
	}
	
	@Test
	public void testaRestGetPersonagem() throws Exception {
		Characters personagem = new Characters();
		personagem.setId(1);
		personagem.setName("Nome");
		db.adicionarPersonagem(personagem);
		MvcResult result = mock.perform(get("/personagens/1")).andExpect(status().isOk()).andReturn();
		ObjectMapper mapper = new ObjectMapper();
		String contentAsString = result.getResponse().getContentAsString();
		Characters retorno = mapper.readValue(contentAsString, Characters.class);
		assertThat(retorno.getName(), Matchers.containsString("Nome"));
	}

	@Test
	public void testaRestPost() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		Characters p = new Characters();
		int id = 1234;
		p.setId(id);
		p.setName("Cumpadi Uoxito");
		p.setResourceURI("http://gateway.marvel.com/v1/public/characters/");
		String json = mapper.writeValueAsString(p);
		mock.perform(post("/personagens").contentType(MediaType.APPLICATION_JSON).content(json))
				.andExpect(status().isOk());
		assertThat(db.obterPersonagemPorId(id), Matchers.notNullValue());
		mock.perform(delete("/personagens/"+id)).andExpect(status().isOk());
		assertThat(db.obterPersonagemPorId(id), Matchers.nullValue());
	}
	
	@Test
	public void testaRestPut() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		int id = 1234;
		
		Characters p = new Characters();
		p.setId(id);
		p.setName("Cumpadi Uoxito");
		p.setResourceURI("http://gateway.marvel.com/v1/public/characters/");
		
		String json = mapper.writeValueAsString(p);
		
		mock.perform(post("/personagens").contentType(MediaType.APPLICATION_JSON).content(json))
				.andExpect(status().isOk());
		assertThat(db.obterPersonagemPorId(id), Matchers.notNullValue());
		assertThat(db.obterPersonagemPorId(id).getName(), Matchers.equalTo("Cumpadi Uoxito"));
		
		p.setName("Epelson Marques");
		json = mapper.writeValueAsString(p);
		
		mock.perform(put("/personagens/"+id).contentType(MediaType.APPLICATION_JSON).content(json))
		.andExpect(status().isOk());
		
		MvcResult result = mock.perform(get("/personagens/"+id)).andExpect(status().isOk()).andReturn();

		String contentAsString = result.getResponse().getContentAsString();
		Characters retorno = mapper.readValue(contentAsString, Characters.class);
		assertThat(retorno.getName(), Matchers.containsString("Epelson Marques"));
		
	}
}
