package br.com.treinamento.dojo.controller;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.treinamento.model.Characters;
import br.com.treinamento.persistencia.Banco;

@SuppressWarnings("rawtypes")
@RestController
public class RestfullController {
	
	@Autowired
	private Banco db;

	@RequestMapping(value = "/personagens", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<List<Characters>> getPersonagens() {
		ResponseEntity<List<Characters>> responseEntity = new ResponseEntity<List<Characters>>(db.obterTodosPersonagens(), HttpStatus.OK);
		return responseEntity;
	}
	
	@RequestMapping(value = "/personagens/{id}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<Characters> getPersonagemPorId(@PathVariable("id") Integer id) {
		HttpStatus status = HttpStatus.OK;
		Characters personagem = db.obterPersonagemPorId(id);
		if (personagem == null) {
			status = HttpStatus.NOT_FOUND;
		}
		ResponseEntity<Characters> responseEntity = new ResponseEntity<Characters>(personagem , status);
		return responseEntity;
	}
	
	@RequestMapping(value = "/personagens", method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON)
	public @ResponseBody ResponseEntity salvarPersonagem(@RequestBody Characters personagem) {
		HttpStatus status = HttpStatus.OK;
		if (personagem == null || personagem.getId() == null) {
			status  = HttpStatus.BAD_REQUEST;
		}else {
			db.adicionarPersonagem(personagem);
		}
		ResponseEntity responseEntity = new ResponseEntity(status);
		return responseEntity;
	}
	
	@RequestMapping(value = "/personagens/{id}", method=RequestMethod.DELETE)
	public @ResponseBody ResponseEntity deletarPersonagem(@PathVariable Integer id) {
		HttpStatus status = HttpStatus.OK;
			db.removerPersonagem(id);
		ResponseEntity responseEntity = new ResponseEntity(status);
		return responseEntity;
	}
	
	@RequestMapping(value = "/personagens/{id}", method=RequestMethod.PUT, produces=MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity atualizarPersonagem(@PathVariable("id") Integer id, @RequestBody Characters personagem) {
		HttpStatus status = HttpStatus.OK;
		db.atualizarPersonagem(id, personagem);
		ResponseEntity responseEntity = new ResponseEntity(status);
		return responseEntity;
	}
}
