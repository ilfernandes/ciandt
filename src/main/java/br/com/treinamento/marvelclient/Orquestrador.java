package br.com.treinamento.marvelclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.treinamento.persistencia.Banco;

@Controller
public class Orquestrador {
	@Autowired
	private Banco db;
	@Autowired
	private MarvelClient client;
	
	@RequestMapping(path="/callMarvel")
	public ResponseEntity<String> callMarvelApi() {
				HttpStatus status = HttpStatus.OK;
		try {
			db.adicionarTodos(client.result("characters"));
			db.adicionarTodos(client.result("comics"));
		} catch (Exception e) {
			e.printStackTrace();
			status = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return new ResponseEntity<>(status);
	}

}
