package br.com.treinamento.marvelclient;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import br.com.treinamento.config.Constants;
import br.com.treinamento.model.Data;
import br.com.treinamento.model.MarvelResponse;
import br.com.treinamento.model.Characters;
import br.com.treinamento.model.Result;

public class MarvelClient {

	private Client client;
	
	

	/**
	 * 
	 */
	public MarvelClient() {
		client = ClientBuilder.newClient(new ClientConfig());
	}

	public List<Result> result(String path) throws Exception {
		long time = new java.util.Date().getTime();
		String hash = generateConnectorHash(time);
		final WebTarget webTarget = client.target(Constants.MARVEL_URL)
				.path(path)
				//.queryParam("nameStartsWith", inicioNome)
				.queryParam("limit", 3).queryParam("ts", time)
				.queryParam("apikey", Constants.API_KEY).queryParam("hash", hash);
				
		
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		
		Response response = invocationBuilder.get();
		MarvelResponse entity = response.readEntity(MarvelResponse.class);
		Data data = entity.getData();
		return data.getResults();
	}
	
	private static String generateConnectorHash(long time) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		java.security.MessageDigest md5 = java.security.MessageDigest.getInstance("md5");
		String stringBytes = time+Constants.PRIVATE_KEY+Constants.API_KEY;
		byte[] hash = md5.digest(stringBytes.getBytes("UTF-8"));
		StringBuilder hexString = new StringBuilder();
		for (byte b : hash) {
		  hexString.append(String.format("%02x", 0xff & b));
		}
	    return hexString.toString();
	}
	
}
