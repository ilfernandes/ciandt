
package br.com.treinamento.model;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;



public class MarvelResponse {

    
    private Integer code;
    
    private String status;
    
    private String copyright;
    
    private String attributionText;
    
    private String attributionHTML;
    
    private String etag;
    
    private Data data;
    
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * 
     *     The code
     */
    
    public Integer getCode() {
        return code;
    }

    /**
     * 
     * 
     *     The code
     */
    
    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     * 
     * 
     *     The status
     */
    
    public String getStatus() {
        return status;
    }

    /**
     * 
     * 
     *     The status
     */
    
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * 
     *     The copyright
     */
    
    public String getCopyright() {
        return copyright;
    }

    /**
     * 
     * 
     *     The copyright
     */
    
    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    /**
     * 
     * 
     *     The attributionText
     */
    
    public String getAttributionText() {
        return attributionText;
    }

    /**
     * 
     * 
     *     The attributionText
     */
    
    public void setAttributionText(String attributionText) {
        this.attributionText = attributionText;
    }

    /**
     * 
     * 
     *     The attributionHTML
     */
    
    public String getAttributionHTML() {
        return attributionHTML;
    }

    /**
     * 
     * 
     *     The attributionHTML
     */
    
    public void setAttributionHTML(String attributionHTML) {
        this.attributionHTML = attributionHTML;
    }

    /**
     * 
     * 
     *     The etag
     */
    
    public String getEtag() {
        return etag;
    }

    /**
     * 
     * 
     *     The etag
     */
    
    public void setEtag(String etag) {
        this.etag = etag;
    }

    /**
     * 
     * 
     *     The data
     */
    
    public Data getData() {
        return data;
    }

    /**
     * 
     * 
     *     The data
     */
    
    public void setData(Data data) {
        this.data = data;
    }

    
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
