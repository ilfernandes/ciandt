
package br.com.treinamento.model;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

public class Price {

    
    private String type;
    
    private String price;
    
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The type
     */
    
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The price
     */
    
    public String getPrice() {
        return price;
    }

    /**
     * 
     * @param price
     *     The price
     */
    
    public void setPrice(String price) {
        this.price = price;
    }

    
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
