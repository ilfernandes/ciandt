
package br.com.treinamento.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

public class Creators {

    
    private String available;
    
    private String returned;
    
    private String collectionURI;
    @JsonIgnore
    private List<Item> items = new ArrayList<Item>();
    
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The available
     */
    
    public String getAvailable() {
        return available;
    }

    /**
     * 
     * @param available
     *     The available
     */
    
    public void setAvailable(String available) {
        this.available = available;
    }

    /**
     * 
     * @return
     *     The returned
     */
    
    public String getReturned() {
        return returned;
    }

    /**
     * 
     * @param returned
     *     The returned
     */
    
    public void setReturned(String returned) {
        this.returned = returned;
    }

    /**
     * 
     * @return
     *     The collectionURI
     */
    
    public String getCollectionURI() {
        return collectionURI;
    }

    /**
     * 
     * @param collectionURI
     *     The collectionURI
     */
    
    public void setCollectionURI(String collectionURI) {
        this.collectionURI = collectionURI;
    }

    /**
     * 
     * @return
     *     The items
     */
    
    public List<Item> getItems() {
        return items;
    }

    /**
     * 
     * @param items
     *     The items
     */
    
    public void setItems(List<Item> items) {
        this.items = items;
    }

    
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
