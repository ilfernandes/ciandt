
package br.com.treinamento.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ 
	"id", 
	"name", 
	"description", 
	"modified", 
	"thumbnail",
	"resourceURI",
	"comics",
	"series",
	"stories",
	"events",
	"urls",
	"additionalProperties"
})
public class Characters extends Result {
	private Integer id;

	private String name;

	private String description;

	private String modified;

	private Thumbnail thumbnail;
	private String resourceURI;

	private Comics_ comics;

	private Series series;

	private Stories stories;

	private Events events;

	private List<Url> urls = new ArrayList<Url>();
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * 
	 * 
	 * The id
	 */

	public Integer getId() {
		return id;
	}

	/**
	 * 
	 * 
	 * The id
	 */

	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 
	 * 
	 * The name
	 */

	public String getName() {
		return name;
	}

	/**
	 * 
	 * 
	 * The name
	 */

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * 
	 * The description
	 */

	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * 
	 * The description
	 */

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 
	 * 
	 * The modified
	 */

	public String getModified() {
		return modified;
	}

	/**
	 * 
	 * 
	 * The modified
	 */

	public void setModified(String modified) {
		this.modified = modified;
	}

	/**
	 * 
	 * 
	 * The thumbnail
	 */

	public Thumbnail getThumbnail() {
		return thumbnail;
	}

	/**
	 * 
	 * 
	 * The thumbnail
	 */

	public void setThumbnail(Thumbnail thumbnail) {
		this.thumbnail = thumbnail;
	}

	/**
	 * 
	 * 
	 * The resourceURI
	 */

	public String getResourceURI() {
		return resourceURI;
	}

	/**
	 * 
	 * 
	 * The resourceURI
	 */

	public void setResourceURI(String resourceURI) {
		this.resourceURI = resourceURI;
	}

	/**
	 * 
	 * 
	 * The comics
	 */

	public Comics_ getComics() {
		return comics;
	}

	/**
	 * 
	 * 
	 * The comics
	 */

	public void setComics(Comics_ comics) {
		this.comics = comics;
	}

	/**
	 * 
	 * 
	 * The series
	 */

	public Series getSeries() {
		return series;
	}

	/**
	 * 
	 * 
	 * The series
	 */

	public void setSeries(Series series) {
		this.series = series;
	}

	/**
	 * 
	 * 
	 * The stories
	 */

	public Stories getStories() {
		return stories;
	}

	/**
	 * 
	 * 
	 * The stories
	 */

	public void setStories(Stories stories) {
		this.stories = stories;
	}

	/**
	 * 
	 * 
	 * The events
	 */

	public Events getEvents() {
		return events;
	}

	/**
	 * 
	 * 
	 * The events
	 */

	public void setEvents(Events events) {
		this.events = events;
	}

	/**
	 * 
	 * 
	 * The urls
	 */

	public List<Url> getUrls() {
		return urls;
	}

	/**
	 * 
	 * 
	 * The urls
	 */

	public void setUrls(List<Url> urls) {
		this.urls = urls;
	}

	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
