package br.com.treinamento.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.fasterxml.jackson.databind.annotation.JsonTypeIdResolver;

import br.com.treinamento.resolvers.ResultTypeResolver;

@JsonTypeInfo(use=Id.CUSTOM, include=As.PROPERTY, property="resourceURI")
@JsonTypeIdResolver(ResultTypeResolver.class)
public  abstract class Result implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4842382026845633838L;

}
