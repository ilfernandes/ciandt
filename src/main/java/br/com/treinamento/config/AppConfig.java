package br.com.treinamento.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;

import br.com.treinamento.dojo.controller.RestfullController;
import br.com.treinamento.marvelclient.MarvelClient;
import br.com.treinamento.marvelclient.Orquestrador;
import br.com.treinamento.persistencia.Banco;

@SpringBootApplication
@ComponentScan(basePackageClasses={RestfullController.class, Orquestrador.class})
public class AppConfig {

	public static void main(String[] args) {
		SpringApplication.run(AppConfig.class, args);
	}
	
	@Bean
	@Scope(value="singleton")
	public Banco getBanco() {
		return new Banco();
	}
	
	@Bean
	public MarvelClient getMarvelClient() {
		return new MarvelClient();
	}
}
