package br.com.treinamento.persistencia;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.treinamento.model.Comics;
import br.com.treinamento.model.Characters;
import br.com.treinamento.model.Result;


public class Banco {
	
	private Map<Integer, Characters> dbPersonagens = new HashMap<>();
	private Map<Integer, Comics> dbComics = new HashMap<>();

	
	public List<Characters> obterTodosPersonagens() {
		return new ArrayList<Characters>(dbPersonagens.values());
	}
	
	
	public Characters adicionarPersonagem(Characters personagem) {
		return dbPersonagens.put(personagem.getId(), personagem);
	}
	
	public Characters atualizarPersonagem(Integer id, Characters personagem) {
		return dbPersonagens.put(id, personagem);
	}

	
	public void removerPersonagem(Integer id) {
		if (id != null) {
			dbPersonagens.remove(id);
		}
	}
	
	
	public void removerPersonagem(Characters personagem) {
		if (personagem != null && personagem.getId() != null) {
			removerPersonagem(personagem.getId());
		}
	}

	
	public Characters obterPersonagemPorId(Integer id) {
		return dbPersonagens.get(id);
	}
	
	
	public void adicionarTodosPersonagens(List<Characters> personagens) {
		for (Characters p: personagens) {
			dbPersonagens.put(p.getId(), p);
		}
	}
	
	public void adicionarTodos(List<Result> results) {
		for (Result r: results) {
			if (r instanceof Characters) {
				Characters p = (Characters)r;
				dbPersonagens.put(p.getId(), p);
			} else if(r instanceof Comics) {
				Comics c = (Comics) r;
				dbComics.put(c.getId(), c);
			}
		}
	}

	
	public void deleteAll() {
		int size = dbPersonagens.size();
		dbPersonagens.clear();
		System.out.print("banco personagens excluido, "+size+" registros afetados");
		size = dbComics.size();
		dbComics.clear();
		System.out.print("banco comics excluido, "+size+" registros afetados");
	}

	public List<Comics> obterTodosComics() {
		return new ArrayList<>(dbComics.values());
	}


	public Comics adicionarComics(Comics comics) {
		return dbComics.put(comics.getId(), comics);
	}


	public void removerComics(Integer id) {
		dbComics.remove(id);
	}


	public void removerComics(Comics comics) {
		if (comics != null && comics.getId() != null) {
			removerComics(comics.getId());
		}		
	}


	public Comics obterPorComicsId(Integer id) {
		return dbComics.get(id);
	}


	public void adicionarTodosComics(List<Comics> comics) {
		for (Comics comic : comics) {
			dbComics.put(comic.getId(), comic);
		}
		
	}

	
	
}
