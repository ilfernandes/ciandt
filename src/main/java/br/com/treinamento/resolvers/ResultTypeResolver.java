package br.com.treinamento.resolvers;

import java.io.IOException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.fasterxml.jackson.databind.DatabindContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.jsontype.TypeIdResolver;
import com.fasterxml.jackson.databind.type.TypeFactory;

public class ResultTypeResolver implements TypeIdResolver {

	private JavaType baseType;

	private String regex = ".*http:\\/\\/gateway\\.marvel\\.com\\/v1\\/public\\/(.*)\\/\\d{0,}";
	private Pattern pattern = Pattern.compile(regex);
	private Properties properties = new Properties();

	@Override
	public void init(JavaType baseType) {
		this.baseType = baseType;
		try {
			properties.load(getClass().getResourceAsStream("/application.properties"));
		} catch (IOException e) {
			throw new RuntimeException("Não acho arquivo de propriedades");
		}
	}

	@Override
	public String idFromValue(Object obj) {
		return idFromValueAndType(obj, obj.getClass());
	}

	@Override
	public String idFromValueAndType(Object obj, Class<?> suggestedType) {
		return obj.getClass().getSimpleName().toLowerCase();
	}

	@Override
	public String idFromBaseType() {
		return "requestUri";
	}

	@Override
	public JavaType typeFromId(DatabindContext context, String id) throws IOException {
		Matcher matcher = pattern.matcher(id);
		Class<?> clazz = null;
		String className = null;
		if (matcher.find()) {
			String group = matcher.group(1);
			className = properties.getProperty(group);
		} else  {
			className = properties.getProperty(id);
		}
		try {
			clazz = Class.forName(className);
		} catch (Exception e) {
			throw new RuntimeException("Classe não suportada");
		}

		return TypeFactory.defaultInstance().constructSpecializedType(baseType, clazz);
	}

	@Override
	public String getDescForKnownTypeIds() {
		return "erro";
	}

	@Override
	public Id getMechanism() {
		return Id.CUSTOM;
	}

}
